# python-mqtt-telegram

This package provides a proxy application between the widespread MQTT protocol (http://mqtt.org/) and the famous Telegram chat application (https://telegram.org/). 
The application is written in python and uses the python Telegram library (https://github.com/python-telegram-bot/python-telegram-bot).
alongside paho-mqtt (https://pypi.org/project/paho-mqtt/). The application has limited but essential functionalities for interacting with MQTT channels. (See **Commands**)

### Prerequirments
- configured MQTT broker with guest access (broker authentication is not supported yet)
- python 2.7
- access to the internet
- At least one working Telegram account
- Registered Telegram bot token (See **Configuration**)

### Commands
- list: listing all available topic values [syntax: `/l`]
- read: reading a topic value [syntax: `/r <topic alias>`]
- write: writing new value to topic: [syntax: `/w <topic alias> <new value>`]
- help: prints help message [syntax `/h`]
- at any wrongly formated command, an error message should be replied

### Authentication 
In the latest version only preconfigured (see "Configuration") users can interact with the bot but they don't need any further authentication.

### Configuration
The configuration file is situated at `/etc/python-mqtt-telegram.conf`
- **token**: As it was mentioned above, you will need a Telegram. Please follow the tutorial to make a bot and get a token (https://www.sohamkamani.com/blog/2016/09/21/making-a-telegram-bot/)
Your token should look like: `270485614:AAHfiqksKZ8WmR2zSjiQ7_v4TMAKdiHm9T0`. 
- **proxy-config**: The proxy configuration is optional and supports only socks5 proxt so far. 
If any of those parameters (proxy-server, proxy-port, proxy-username, proxy-password) are missing, all the configurations are ignored and configured without proxy.
- **mqtt-config**: MQTT broker IP address and port
- **Inventory**: Here are the preconfigured MQTT topcis that will be signed up by the application. 
  - **alias**: Short name of topic, that will be used inside the application, e.g: kitchen-lamp, doorbell
  - **topic**: Full path of MQTT topic, that is used by the broker e.g: /devices/buzzer/controls/frequency
  - **read_only**: boolean parameter, if we allow user to write these topics. If read_only=true but the user attempts to use /w command, the applications returns an error message.
  - **update_on_change**: boolean parameter, if we want to send notification at any change on the topic. (not advised for frequently changing values, as temperature sensors)
- **telegram-users**: Enabled Telegram users. Only these users can interact with the bot, others users get an error message.


```json
{
    "token": "my-token",
    "proxy-server": "socks5://address.com",
    "proxy-port": 443,
    "proxy-username": "telegram",
    "proxy-password": "telegram",
    "mqtt-broker-address" : "localhost",
    "mqtt-broker-port" : "18883",
    "inventory": [
    {
        "alias": "frequency",
        "topic": "/devices/buzzer/controls/frequency",
        "read_only": false,
        "update_on_change": true
    },
    {
        "alias": "volume",
        "topic": "/devices/buzzer/controls/volume",
        "read_only": true,
        "update_on_change": false
    },
    {
        "alias": "volt",
        "topic": "/devices/wb-gpio/controls/5V_OUT",
        "read_only": false,
        "update_on_change": false
        }
    ],
    "telegram-users": ["asd", "troublesoup"]
}
```
### How to start application
First download `python-mqtt-telegram_<version>_all.deb` from https://gitlab.com/attiladoor/mqtt-telegram/tags and run: 
```bash
sudo dpkg -i python-mqtt-telegram_<version>_all.deb
sudo apt-get install -f
```
In the current version, it will install python-pip and many other package (it will be fixed soon). If it is successfuly installed, edit the `/etc/python-mqtt-telegram.conf` 
in your favor and run (if --config is not used, the default /etc/python-mqtt-telegram.conf is used)
```bash
/usr/bin/python-mqtt-telegram --config /etc/python-mqtt-telegram.conf
```
or to run it in the background, just simply type:
```bash
service python-mqtt-telegram start
```

(the service is launched at bootup by default)