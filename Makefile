LIBDIR=/usr/lib/python-mqtt-telegram

.PHONY: all clean

all:
clean :

install: all

	git submodule init
	git submodule update --recursive

	install -d $(DESTDIR)
	install -d $(DESTDIR)/etc
	install -d $(DESTDIR)/usr
	install -d $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/lib
	install -d $(DESTDIR)/$(LIBDIR)

	install -m 0755 python-mqtt-telegram.py $(DESTDIR)/$(LIBDIR)/
	cp -r python-telegram-bot  	    		$(DESTDIR)/$(LIBDIR)/python-telegram-bot
	cp -r paho.mqtt.python  		  	  	$(DESTDIR)/$(LIBDIR)/paho.mqtt.python
	cp -r PySocks   				  	  	$(DESTDIR)/$(LIBDIR)/PySocks   

	ln -s  $(LIBDIR)/python-mqtt-telegram.py $(DESTDIR)/usr/bin/python-mqtt-telegram

	install -m 0644  config.json $(DESTDIR)/etc/python-mqtt-telegram.conf
