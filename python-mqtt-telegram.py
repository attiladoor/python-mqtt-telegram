#!/usr/bin/env python
# -*- coding: utf-8 -*-


from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import logging
import json
import time
import paho.mqtt.client as mqtt
import argparse

# Enable logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.WARNING)

logger = logging.getLogger(__name__)

TELEGRAM_CHAT_IDS_FILE = "telegram-chat-ids.json"

##GLOBAL VARIABLES##
INVENTORY = dict()
MQTT_VALUE_STORAGE = dict()
TELEGRAM_USERS = ""
TELEGRAM_TOKEN = ""
TELEGRAM_PROXY_ADDRESS = ""
TELEGRAM_PROXY_USERNAME = ""
TELEGRAM_PROXY_PASSWORD = ""
TELEGRAM_PROXY_CONFIGURED = False
MQTT_BROKER_ADDR = ""
MQTT_BROKER_PORT = ""
TELEGERAM_UPDATER = ""
TELEGRAM_CHAT_IDS = dict()

##TELEGRAM_CHAT_ID_HELPER##
def add_chat_id(new_id, user_name):
    """
    Adds a new chat ID to a list where IDs are stored aside with user names. If the the ID is in the list already, it return. If 
    There is a new ID, it adds to the list and save it into a file as json, The file name is controlled by a global variable. 

    Args:
        new_id (:obj:`int` | :obj:`str`): chat id of a telegram conversation (unique for each user)
        user_name (:obj:`str`): telegram username
    Returns:
        None
    """
    if new_id in list(TELEGRAM_CHAT_IDS.keys()):
        return
    else:
        TELEGRAM_CHAT_IDS[new_id] = user_name
        json_ids = [{"id": i, "username": u} for i, u in TELEGRAM_CHAT_IDS.iteritems()]
        with open(TELEGRAM_CHAT_IDS_FILE,"w") as f:
            f.write(json.dumps(json_ids))
        f.close()

def load_chat_ids():
    """
    Loads the saved Telegram chat IDs and user names from json file into a dictionary. The file name is set
    by a global variable. 
    Note: it updates TELEGRAM_CHAT_IDS

    Args:
        None
    Returns:
        None
    """
    try:
        with open(TELEGRAM_CHAT_IDS_FILE, "r") as f:
            data = json.load(f)
            for entity in data:
                TELEGRAM_CHAT_IDS[entity["id"]] = entity["username"]
        f.close()
    except Exception as e:
        logger.warning(TELEGRAM_CHAT_IDS_FILE + " is missing")

##JSON##
class MqttEntity:    
    """
    Class for storing atributes of each MQTT topic.
    Note: It doesn't have any member functions
    """
    def __init__(self, alias, topic, read_only, update_on_change):
        self.alias = alias
        self.topic = topic
        self.read_only = read_only
        self.update_on_change = update_on_change


def read_json_config(config_path):
    """
    Reads config json file and load configuration into the selected variables
    It reads either Telegram configs as: bot token, optional proxy settings, allowed telegram users
    and MQTT configs as: broker config, topic config    

    Args:
        config_path (:obj:`str`): path to the configuration file. By default it should be /etc/python-mqtt-telegram.json
    Returns:
        None
    """
    with open(config_path) as f:
        data = json.load(f)
    ## MQTT connection settings    
    global MQTT_BROKER_ADDR
    global MQTT_BROKER_PORT
    MQTT_BROKER_ADDR = data["mqtt-broker-address"]
    MQTT_BROKER_PORT = data["mqtt-broker-port"]

    ## Telegram settings
    global TELEGRAM_TOKEN 
    TELEGRAM_TOKEN = data['token']

    ## Telegram proxy settings
    try:
        global TELEGRAM_PROXY_ADDRESS
        global TELEGRAM_PROXY_USERNAME
        global TELEGRAM_PROXY_PASSWORD
        global TELEGRAM_PROXY_CONFIGURED

        TELEGRAM_PROXY_ADDRESS = data['proxy-server'] + ":"
        TELEGRAM_PROXY_ADDRESS += str(data['proxy-port'])
        TELEGRAM_PROXY_USERNAME = data['proxy-username']
        TELEGRAM_PROXY_PASSWORD = data['proxy-password']
        TELEGRAM_PROXY_CONFIGURED = True

    except Exception as e: 
        logger.warning("Error at parsing proxy parameter:" + str(e))
        logger.warning("Continue without proxy")
        
    ## filling mqtt topic INVENTORY
    for obj in data['inventory']:
        INVENTORY[obj['alias']] = MqttEntity(obj['alias'], obj['topic'], obj['read_only'], obj['update_on_change'])

    ## User settings
    global TELEGRAM_USERS
    TELEGRAM_USERS = data['telegram-users']

##MQTT##
MQTT_CONNECTED = False
mqtt_client = mqtt.Client("Telegram", transport='websockets')

def mqtt_connect(mqtt_address, mqtt_port):
    """
    Establishes connection to MQTT broker
    
    Args:
        mqtt_address (:obj:`str`): mqtt broker address as string (standard IP format, e.g: 192.168.0.123)
        mqtt_port (:obj:`int`): mqtt broker port as integer (e.g; 8883)
    Returns:
        None
    """
    mqtt_client.on_connect = on_mqtt_connect
    mqtt_client.on_message = on_mqtt_message
    mqtt_client.on_publish = on_mqtt_publish

    mqtt_client.connect(mqtt_address, port=mqtt_port)
    mqtt_client.loop_start()

    while MQTT_CONNECTED != True:
        time.sleep(0.1)

    for alias, entity in INVENTORY.items():
        logger.info("MQTT Subscribe to: " + entity.topic)
        mqtt_client.subscribe(entity.topic)

def on_mqtt_connect(client, userdata, flags, rc):
    """
    Callback function at successful connection
    Parameters are mandatory and required by PAHO library
    Args:
        client: the client instance for this callback
        userdata: the private user data as set in Client() or user_data_set()
        flags: response flags sent by the broker
        rc: return value of connection establishment
    Returns:
        None
    Raises:
        Exit signal if connection establishment has failed
    """
    if rc == 0:
        global MQTT_CONNECTED
        MQTT_CONNECTED = True
        logger.info("MQTT successfuly connected to broker")
    else:
        logger.info("MQTT failed to connect to broker")
        sys.exit(1)
        
def on_mqtt_publish(client, userdata, mid):
    """
    Callback function at successful publish method

    Parameters are mandatory and required by PAHO library
    Args:
        client: the client instance for this callback
        userdata: the private user data as set in Client() or user_data_set()
        mid: The mid variable matches the mid variable returned from 
             the corresponding publish() call, to allow outgoing messages to be tracked.
    Returns:
        None
    """
    logger.info("MQTT - publish successful - " + str(mid))

def on_mqtt_message(client, userdata, message):
    """
    Callback function at any change on the subscribed mqtt topics.
    It reads the topic value and store into an internal container that keeps all the actual topic values.
    Additionally, it send a notification at those topics that are marked as "update on change". 
    Notifications requires the chat id of each active conversations.

    Args:
        client: the client instance for this callback
        userdata: the private user data as set in Client() or user_data_set()
        message: an instance of MQTTMessage. This is a class with members topic, payload, qos, retain.
    Returns:
        None
    """
    logger.info("MQTT - " + message.topic + " " + message.payload)
    for alias, entity in INVENTORY.items():
        if entity.topic == message.topic:
            MQTT_VALUE_STORAGE[entity.alias] = message.payload            
            if entity.update_on_change == True and not TELEGERAM_UPDATER == "":
                for id, user in TELEGRAM_CHAT_IDS.iteritems():
                    if check_telegram_user(user):
                        message_to_send = entity.alias + " - " + message.payload
                        TELEGERAM_UPDATER.bot.send_message(chat_id=id, text=message_to_send)
                        logger.info("NOTIFICATION: " + user + " - messgae: " + message_to_send)            
#######
def autorization_handler(update):
    """
    Handles Telegram user authentication and uploads chat ID's list
    Sends error reply to user if authentication has failed.
    Args:
        update: Telegram updater object, containing all message updates
    Returns:
        True if authentication has been succesfull
        False if user is not enabled 
    """
    if(check_telegram_user(update.message.from_user.username)):
        add_chat_id(update.message.chat.id ,update.message.from_user.username)
        return True
    else:
        update.message.reply_text('Unauthorized user!')
        logger.warning("Unauthorized user attempt!:" + update.message.from_user.username)
        return False

def check_telegram_user(user):
    """
    Checks if the given telegram user is member of allowed users group

    Args:
        user (:obj:`str`): telegram user name
    Returns:
        True if user is member of the list
        False if not
    """
    if user in TELEGRAM_USERS:
        return True
    else:
        return False
        
def get_help_message():
    """
    Factory function for making help telegram message
    
    It uses "INVENTORY" global variable to reveal the available mqtt topic aliases
    Args:
        None
    Returns:
        help message as string
    """
    message = 'Available topics:\n'
    for i in list(INVENTORY.keys()):
        if INVENTORY[i].read_only:
            message += i + "- read only\n"
        else:
            message += i + '\n'
    message += 'Available commands:\n/w <topic> <value> -write topic\n/r <topic> -read topic\n/l -list all topic values\n/h -get help\n'
    return message

def dummy_handler(bot, update):
    """
    Handler for unrecognized commands. It simply sends a help message as reply.
    Args:
        bot: Telegram Bot object. Needed by telegram handler
        update: Telegram update object. Needed by telegram handler
    Returns:
        None
    """
    if(not autorization_handler(update)):
        return

    logger.info("DUMMY: %s - %s", update.message.from_user.username, update.message.text)
    update.message.reply_text('invalid command, write /h for help')
    return

def write_handler(bot, update):
    """
    Write command handler. Parses write command from Telegram message and publishes the new value onto MQTT topic.
    Args:
        bot: Telegram Bot object. Needed by telegram handler
        update: Telegram update object. Needed by telegram handler
    Returns:
        None
    """
    if(not autorization_handler(update)):
        return
    
    logger.info("WRITE: %s - %s", update.message.from_user.username, update.message.text)
    msg = update.message.text.split(' ')
    if len(msg) != 3:
        update.message.reply_text('invalid message format')
        logger.info("ERROR: invalid message format:" + update.message.text)
    elif msg[1] not in list(INVENTORY.keys()):
        update.message.reply_text('invalid alias')
        logger.info("ERROR: invalid alias:" + update.message.text)
    else:
        logger.info(INVENTORY[msg[1]].topic + " " + msg[2])
        if INVENTORY[msg[1]].read_only:
            update.message.reply_text('read only topic')
            logger.info("ERROR: read only topic:" + update.message.text)
        else:
            res = mqtt_client.publish(str(INVENTORY[msg[1]].topic),payload=str(msg[2]),retain=True)
            if res[0]:
                update.message.reply_text('failed to write')
    return

def read_handler(bot, update):
    """
    Read command handler. Parses read command from Telegram message and sends the cached mqtt topic value as reply.
    It does not initiate MQTT topic reading. 
    Args:
        bot: Telegram Bot object. Needed by telegram handler
        update: Telegram update object. Needed by telegram handler
    Returns:
        None
    """
    if(not autorization_handler(update)):
        return

    logger.info("READ: %s - %s", update.message.from_user.username, update.message.text)
    msg = update.message.text.split(' ')
    if len(msg) != 2:
        update.message.reply_text('invalid message format')
        logger.info("ERROR: invalid message format:" + update.message.text)
    elif msg[1] not in list(INVENTORY.keys()):
        update.message.reply_text('invalid alias')
        logger.info("ERROR: invalid alias:" + update.message.text)
    else:
        update.message.reply_text(MQTT_VALUE_STORAGE[msg[1]])
    return

def list_handler(bot, update):
    """
    List command handler. Sends the cached mqtt topic values as Telegram message reply that
    includes all available topic content. It does not initiate MQTT topic reading. 
    Args:
        bot: Telegram Bot object. Needed by telegram handler
        update: Telegram update object. Needed by telegram handler
    Returns:
        None
    """
    if(not autorization_handler(update)):
        return
    
    logger.info("LIST: %s - %s", update.message.from_user.username, update.message.text)
    message = ""
    for alias, value in MQTT_VALUE_STORAGE.items():
        message += alias + ": " + value + "\n"
    update.message.reply_text(message)
    return

def help_handler(bot, update):
    """
    Help command handler. It sends back the helper Telegram message.
    Args:
        bot: Telegram Bot object. Needed by telegram handler
        update: Telegram update object. Needed by telegram handler
    Returns:
        None
    """
    if(not autorization_handler(update)):
        return

    logger.info("HELP: %s - %s", update.message.from_user.username, update.message.text)
    update.message.reply_text(get_help_message())
    return

def error(bot, update, error):
    """
    Log Errors caused by Updates.
    """
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
      '--config',
      type=str,
      default='/etc/python-mqtt-telegram.conf',
      help='Path to configuration file'
    )

    args = parser.parse_args()
    # reading configuration
    read_json_config(args.config)
    # establish mqtt connection
    mqtt_connect(MQTT_BROKER_ADDR, int(MQTT_BROKER_PORT))
    # loading cached chat ID-s with user names from file
    load_chat_ids()

    global TELEGERAM_UPDATER
    if TELEGRAM_PROXY_CONFIGURED:
        REQUEST_KWARGS={
        'proxy_url': TELEGRAM_PROXY_ADDRESS,
        'urllib3_proxy_kwargs': {
            'username': TELEGRAM_PROXY_USERNAME,
            'password': TELEGRAM_PROXY_PASSWORD,
        }
        }
        TELEGERAM_UPDATER = Updater(token=TELEGRAM_TOKEN, request_kwargs=REQUEST_KWARGS)
    else:
        TELEGERAM_UPDATER = Updater(token=TELEGRAM_TOKEN)

    # Get the dispatcher to register handlers
    dp = TELEGERAM_UPDATER.dispatcher
    dp.add_handler(CommandHandler('l', list_handler))
    dp.add_handler(CommandHandler('w', write_handler))
    dp.add_handler(CommandHandler('r', read_handler))
    dp.add_handler(CommandHandler('h', help_handler))
    dp.add_handler(MessageHandler(Filters.text, dummy_handler))
    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    TELEGERAM_UPDATER.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    TELEGERAM_UPDATER.idle()


if __name__ == '__main__':
    main()
